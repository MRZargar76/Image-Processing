import numpy as np
from KMeans import KMeans
from convert import twoD_to_hyper


class SLIC(KMeans):
    '''خوشه بندی سوپر پیکسل ها به روش SLIC
    به گونه ای میتوان گفت این روش از تعمیم یافتن روش kmeans حاصل شده است
    .
    ####  نحوه استفاده  ####
    img = SLIC(img).fit()
    .
    ####  ورودی ها  ####
    img:
    تصویر ورودی
    '''
    def __init__(self, img):
        if img.ndim < 2:
            raise 'image shape must be similar to (m, n[, k])'
        
        # به باند های تصویر دو باند اندیس موقعیت هر پیکسل راستای ایکس و ایگری اضافه میشود
        img = self.append_xy(img)
        super().__init__(img)

        self.c = np.max(img)


    def append_xy(self, img):
        '''به باند های تصویر دو باند اندیس موقعیت هر پیکسل راستای ایکس و ایگری اضافه میشود
        .
        ####  ورودی ها  ####
        img:
        تصویر ورودی
        '''

        # محاسبه ابعاد تصویر
        shape = img.shape
        # اگر تصویر دوبعدی باشد و یا از یک باند تشکیل شده باشد، ابعاد آن به صورت مجازی سه بعدی میشود
        if img.ndim == 2:
            shape += (1,)
        
        # یک ماتریس خالی بع ابعاد مورد نیاز ایجاد میشود
        new_img = np.empty((*shape[:2], shape[2] + 2))
        # محاسبه اندیس های هر پیکسل در راستاهای ایکس و ایگری
        y, x = np.meshgrid(range(shape[1]), range(shape[0]))

        # مقدار دهی ماتریس ایجاد شده
        new_img[:,:,0] = x
        new_img[:,:,1] = y
        new_img[:,:,2:] = img

        return new_img


    def init_centers(self):
        '''مقدار دهی اولیه مراکز دسته
        '''
        # محاسبه ابعاد تصویر
        w, h, feature_count = self.shape

        # محاسبه فاصله گرید ها
        self.s = np.sqrt(self.pixel_count / self.k)
        
        # محاسبه اندیس مراکز دسته ها با توجه فاصله گرید بندی
        xIdx = np.ceil(np.arange(self.s/2, w, self.s))
        yIdx = np.ceil(np.arange(self.s/2, h, self.s))
        
        # محاسبه اندیس های شروع و پایان مربع همسایگی
        xFrom, xTo = xIdx - self.s, xIdx + self.s
        yFrom, yTo = yIdx - self.s, yIdx + self.s

        xFrom[0] = 0
        yFrom[0] = 0
        xTo[-1] = w
        yTo[-1] = h

        self.centers = []
        pixels_idx_as_2d = twoD_to_hyper(self.pixels_idx, w, h)
        for i in range(len(xIdx)):
            for j in range(len(yIdx)):
                x, y = map(int, (xIdx[i], yIdx[j]))

                # تعیین مقادیر و پیکسل های همسایه مرکز دسته
                self.centers.append(
                    self.Center(
                        self.img[y*w + x],
                        pixels_idx_as_2d[int(xFrom[i]):int(xTo[i]), int(yFrom[j]):int(yTo[j])].ravel()
                    )
                )
        
        # از آنجا که مراکز دسته به صورت گرید بندی بر روی تصویر پخش میشود ممکن است از تعداد مرکز دسته ورودی متفاوت باشد
        self.k = len(self.centers)

    # همسایگی های هر دسته همانند KMeans ثابت فرض شده است
    # def get_update_neighbors(self, center):
    #     return super().get_update_neighbors(center)


    def dist(self, center, neighbors):
        '''محاسبه فاصله پیکسل های همسایه از مرکز دسته
        .
        ####  ورودی ها  ####
        center:
        مقادیر مرکز دسته
        neighbors:
        پیکسل های همسایه
        '''
        # فاصله ی باند های اصلی تصویر
        # فاصله = فاصله اقلیدسی
        term1 = np.sqrt(np.sum((center[:2] - neighbors[:,:2])**2, axis=1))
        # فاصله ی مکانی
        # فاصله = فاصله اقلیدسی
        term2 = np.sqrt(np.sum((center[2:] - neighbors[:,2:])**2, axis=1))

        # فاصله = جذر مجموع مربعات فاصله مکانی و فاصله باند های اصلی تصویر
        return np.sqrt((term1/self.s)**2 + (term2/self.c)**2)


if __name__ == "__main__":
    import matplotlib.pyplot as plt
    
    img = plt.imread('./data/me.jpg')
    r = SLIC(img).fit(k=100)[:,:,2:]
    
    plt.figure()
    ax = plt.subplot(121)
    plt.imshow(img)
    ax.set_title('orginal image')
    ax.axes.get_xaxis().set_ticks([])
    ax.axes.get_yaxis().set_ticks([])
    plt.box(True)
    ax = plt.subplot(122)
    plt.imshow(r)
    ax.set_title("SLIC")
    ax.axes.get_xaxis().set_ticks([])
    ax.axes.get_yaxis().set_ticks([])
    plt.box(True)

    plt.show()
