import warnings
import numpy as np
from Canny import Canny
from convert import to_gray
from Filters import Gaussian
from Morphology import Morphology


# Copied from https://github.com/Lornatang/ESRGAN-PyTorch
def _check_image(raw_image: np.ndarray, dst_image: np.ndarray):
    """Check whether the size and type of the two images are the same

    Args:
        raw_image (np.ndarray): image data to be compared, BGR format, data range [0, 255]
        dst_image (np.ndarray): reference image data, BGR format, data range [0, 255]

    """
    # check image scale
    assert raw_image.shape == dst_image.shape, \
        f"Supplied images have different sizes {str(raw_image.shape)} and {str(dst_image.shape)}"

    # check image type
    if raw_image.dtype != dst_image.dtype:
        warnings.warn(f"Supplied images have different dtypes{str(raw_image.shape)} and {str(dst_image.shape)}")


# Copied from https://github.com/Lornatang/ESRGAN-PyTorch
def PSNR(raw_image: np.ndarray, dst_image: np.ndarray):
    """Python implements PSNR (Peak Signal-to-Noise Ratio, peak signal-to-noise ratio) function

    Args:
        raw_image (np.ndarray): image data to be compared, BGR format, data range [0, 255]
        dst_image (np.ndarray): reference image data, BGR format, data range [0, 255]

    Returns:
        psnr_metrics (np.float64): PSNR metrics

    """
    # Check if two images are similar in scale and type
    _check_image(raw_image, dst_image)

    # Convert data type to numpy.float64 bit
    raw_image = raw_image.astype(np.float64)
    dst_image = dst_image.astype(np.float64)

    psnr_metrics = 10 * np.log10(1. / np.mean((raw_image - dst_image) ** 2) + 1e-8)

    return psnr_metrics


def SSIM(raw_image: np.ndarray, dst_image: np.ndarray):
    _check_image(raw_image, dst_image)

    raw_image = to_gray(raw_image)
    dst_image = to_gray(dst_image)

    c1 = (0.01 * 255.0) ** 2
    c2 = (0.03 * 255.0) ** 2

    kernel = {
        'sigma': 1.5,
        'kernel_size': 11
    }

    raw_mean = Gaussian(raw_image, **kernel)
    dst_mean = Gaussian(dst_image, **kernel)
    raw_mean_square = raw_mean ** 2
    dst_mean_square = dst_mean ** 2
    raw_dst_mean = raw_mean * dst_mean
    raw_variance = Gaussian(raw_image**2, **kernel) - raw_mean_square
    dst_variance = Gaussian(dst_image**2, **kernel) - dst_mean_square
    raw_dst_covariance = Gaussian(raw_image * dst_image, **kernel) - raw_dst_mean

    ssim_molecular = (2 * raw_dst_mean + c1) * (2 * raw_dst_covariance + c2)
    ssim_denominator = (raw_mean_square + dst_mean_square + c1) * (raw_variance + dst_variance + c2)

    ssim_metrics = ssim_molecular / ssim_denominator
    ssim_metrics = np.mean(ssim_metrics)

    return ssim_metrics


def UQI(raw_image: np.ndarray, dst_image: np.ndarray):
    _check_image(raw_image, dst_image)

    raw_std = np.std(raw_image, ddof=1)
    dst_std = np.std(dst_image, ddof=1)
    raw_mean  = np.mean(raw_image)
    dst_mean  = np.mean(dst_image)
    raw_dst_cov = np.cov(raw_image.flatten(), dst_image.flatten())
    raw_dst_cov = np.sqrt(raw_dst_cov[0,1])

    return 4 * raw_dst_cov * raw_mean * dst_mean / ((raw_mean**2 + dst_mean**2) * (raw_std**2 + dst_std**2))


def SAM(raw_image: np.ndarray, dst_image: np.ndarray):
    _check_image(raw_image, dst_image)

    return np.arccos(np.dot(raw_image.ravel(), dst_image.ravel()) / np.linalg.norm(raw_image) / np.linalg.norm(dst_image))


def RMSE(raw_image: np.ndarray, dst_image: np.ndarray):
    _check_image(raw_image, dst_image)

    return np.sqrt(np.sum((raw_image - dst_image)**2) / (raw_image.size - 1))


def CC(raw_image: np.ndarray, dst_image: np.ndarray):
    _check_image(raw_image, dst_image)

    return np.corrcoef(raw_image.ravel(), dst_image.ravel())[0, 1]


def SDD(raw_image: np.ndarray, dst_image: np.ndarray):
    _check_image(raw_image, dst_image)

    return np.std(raw_image - dst_image, ddof=1)


def RGIndex(raw_image: np.ndarray, dst_image: np.ndarray, index_method=UQI, edge=None):
    '''RG Index Algorithm

    Published in: Arabian Journal of Geosciences, vol. 9, no. 1, p. 45, Jan. 2016.
    Title: "Quality assessment of pan-sharpening methods in high-resolution
    satellite images using radiometric and geometric index"
    Authors: M. Hasanlou and M. R. Saradjian

    Overall explanation. This code focuses on quality assessment of pansharpening of multispectral (MS)
    images with high-resolution panchromatic (Pan) images. Since most existing quality
    assessments take the entire image into account simultaneously and generate some uncertainties,
    a novel and rather objective quality index has been proposed for image fusion.
    The index is comprised of geometric and radiometric parts. Both geometric and radiometric measurements are calculated
    using morphological algorithm applied on an edge image to create a mask which is used to separate highfrequency regions
    from low-frequency ones.The accuracy assessment is made using common existing criteria on geometric and radiometric segments,
    and then a weighted sum is calculated to generate radiometric and geometric index (RG index).

    Copyright (2017) School of Surveying and Geospatial Engineering, 
                     College of Engineering, University of Tehran, Iran.
    '''

    _check_image(raw_image, dst_image)

    raw_image = to_gray(raw_image)
    dst_image = to_gray(dst_image)

    if edge:
        _check_image(raw_image, edge)
    else:
        edge = Canny(
            T_H=0.02,
            T_L=0.4*0.02,
            sigma=np.sqrt(2)
        ).fit(dst_image)

        edge = Morphology(edge).Dilation(
            kernel=[[None, 1, None],
                    [   1, 1,    1],
                    [None, 1, None]]
        )

    edge = edge.astype(bool)

    raw_image_Geometric = raw_image[edge]
    dst_image_Geometric = dst_image[edge]

    raw_image_Radiometric = raw_image[np.logical_not(edge)]
    dst_image_Radiometric = dst_image[np.logical_not(edge)]

    wr = raw_image_Radiometric.size / raw_image.size
    wg = raw_image_Geometric.size / raw_image.size

    ig = index_method(raw_image_Geometric, dst_image_Geometric)
    ir = index_method(raw_image_Radiometric, dst_image_Radiometric)

    return wr * ir + wg * ig


if __name__ == '__main__':
    import matplotlib.pyplot as plt
    
    img = plt.imread('./data/me.jpg')
    r = RGIndex(img, img, RMSE)

    print(r)
