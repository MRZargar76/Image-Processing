import numpy as np
from common import convolve
from common import get_neighbors_as_shift_images


def gussian2D(x, y, sigma=1):
    '''محاسبه گوسین
    ####  ورودی ها  ####
    x, y:
    موقعیت پیکسل
    sigma:
    انحراف معیار
    '''
    return np.exp(-(x**2 + y**2)/(2 * sigma**2)) / (2 * np.pi * sigma**2)


def sobel_kernels():
    '''محاسبه کرنل سوبل
    '''
    Kx = np.array([[-1, 0, 1], [-2, 0, 2], [-1, 0, 1]], np.float32)
    Ky = np.array([[1, 2, 1], [0, 0, 0], [-1, -2, -1]], np.float32)

    return Kx, Ky


def laplacian_kernel(coef=1, mode=0):
    '''محاسبه کرنل لاپلاسین
    ####  ورودی ها  ####
    coef:
    اگر منفی باشد قرینه کرنل بر میگردد
    mode:
    اگر فرد باشد در امتداد قطر های کرنل نیز مقدار وجود دارد
    '''
    coef = 1 if coef >= 0 else -1
    mode = 0 if abs(mode) % 2 == 0 else 1 
    
    if mode == 0:
        K = np.array([[0, 1, 0], [1, -4, 1],[0, 1, 0]], np.float32)
    elif mode == 1:
        K = np.array([[1, 1, 1], [1, -8, 1],[1, 1, 1]], np.float32)

    return coef * K


def gaussian_kernel(sigma=1, size=5):
    '''محاسبه کرنل گوسین
    ####  ورودی ها  ####
    sigma:
    انحراف معیار
    size:
    ابعاد کرنل
    '''
    half_size = size // 2
    rng = range(-half_size, half_size + 1)
    
    x, y = np.meshgrid(rng, rng)
    
    kernel = gussian2D(x, y, sigma)
    return kernel


def high_pass_kernel(sze, cutoff, n):
    # HIGHPASSFILTER  - Constructs a high-pass butterworth filter.
    #
    # usage: f = highpassfilter(sze, cutoff, n)
    #
    # where: sze    is a two element vector specifying the size of filter
    #               to construct.
    #        cutoff is the cutoff frequency of the filter 0 - 0.5
    #        n      is the order of the filter, the higher n is the sharper
    #               the transition is. (n must be an integer >= 1).
    #
    # The frequency origin of the returned filter is at the corners.
    #
    # See also: LOWPASSFILTER, HIGHBOOSTFILTER, BANDPASSFILTER
    #

    # Peter Kovesi   pk@cs.uwa.edu.au
    # Department of Computer Science & Software Engineering
    # The University of Western Australia
    #
    # October 1999

    return 1.0 - low_pass_filter(sze, cutoff, n)


def low_pass_kernel(sze, cutoff, n):
    # LOWPASSFILTER - Constructs a low-pass butterworth filter.
    #
    # usage: f = lowpassfilter(sze, cutoff, n)
    #
    # where: sze    is a two element vector specifying the size of filter
    #               to construct.
    #        cutoff is the cutoff frequency of the filter 0 - 0.5
    #        n      is the order of the filter, the higher n is the sharper
    #               the transition is. (n must be an integer >= 1).
    #
    # The frequency origin of the returned filter is at the corners.
    #
    # See also: HIGHPASSFILTER, HIGHBOOSTFILTER, BANDPASSFILTER
    #

    # Peter Kovesi   pk@cs.uwa.edu.au
    # Department of Computer Science & Software Engineering
    # The University of Western Australia
    #
    # October 1999
    #
    # Modified Rob Gaddi   gaddi@rice.edu
    # ELEC 301
    # Rice University
    #
    # December 2001

    if cutoff < 0 or cutoff > 0.5:
        raise Exception('cutoff frequency must be between 0 and 0.5')

    if n % 1 != 0 or n < 1:
        raise Exception('n must be an integer >= 1')

    #  Modification ELEC 301 Project Group, Dec 2001
    #  Original code [rows, cols] = sze was not accepted by Matlab
    rows = sze[0]
    cols = sze[1]

    # X and Y matrices with ranges normalised to +/- 0.5
    x =  (np.ones((rows,1)) @ np.arange(1, cols+1).reshape(1,-1) - (np.fix(cols/2)+1))/cols
    y =  (np.arange(1, rows+1).reshape(1,-1).T @ np.ones((1,cols)) - (np.fix(rows/2)+1))/rows

    radius = np.sqrt(x**2 + y**2)        # A matrix with every pixel = radius relative to centre.

    #  Alteration, ELEC 301 Project Group, Dec 2001
    #  Original code fftshifted the filter before output.  Since
    #  imFFT and imIFFT already shift, the output should remain low-centered.
    return 1 / (1.0 + (radius / cutoff)**(2*n))   # The filter


def imfft(X):
    return np.fft.fftshift(np.fft.fft2(X))


def imifft(X):
    return np.abs(np.fft.ifft2(np.fft.ifftshift(X)))


def zero_crossing(img):
    '''جهت انجام فرایند لبه یابی، این عملیات انجام میشود.
    .
    ####  ورودی ها  ####
    img:
    تصویر خروجی فیلتر لاپلاسینِ گوسین
    '''
    # محاسبه همسایه های هر یک از پیکسل ها
    neighbors = get_neighbors_as_shift_images(img)
    # مقدار مرکزی
    value = neighbors[1,1]

    is_edge = np.zeros(value.shape, dtype=bool)
    maximum = np.ones(value.shape) * -np.inf
    minimum = np.ones(value.shape) * np.inf

    # لبه در تصویر خروجی فیلتر لاپلاسین گوسین از منفی به مثبت و یا بر عکس جا به جا میشود
    for i in range(3):
        for j in range(3):
            neighbor = neighbors[i, j]
            
            has_positive = neighbor > 0
            has_negative = neighbor < 0

            # بررسی میشود که در اطراف خود مقدار مثبت یا منفی داشته باشد
            # در نتیجه به عنوان لبه احتمالی در نظر گرفته میشود
            is_edge = np.logical_or(is_edge, has_positive, has_negative)
 
            # مقدار یبشیسنه و کمینه در همسایگی هر تصویر محاسبه میشود
            minimum = np.where(neighbor < minimum, neighbor, minimum)
            maximum = np.where(neighbor > maximum, neighbor, maximum)
    
    # پیکسلی که به عنوان لبه در نظر گرفته میشود که:
    # اولا به عنوان پیکسل لبه احتمالی در نظر گرفته شده باشد
    # و دوم مقدار مرکزی صفر نباشد
    # در نهایت قدر مطلق مقدار پیکسل لبه با بزرگترین قدر مطلق همسایه خود جمع میشود
    z = np.where(np.logical_not(is_edge), 0,
        np.where(value == 0, 0,
        np.where(value > 0, value + np.abs(minimum), np.abs(value) + maximum)))

    # مقادیر پیکسل ها بین صفر تا یک آورده میشود
    z /= z.max()
    return z


# -----------------------------------------------------


def Negative(img):
    '''از تصویر درجه خاکستری ورودی، تصویر نگاتیو آن را بر می گرداند.
    .
    #### نحوه استفاده ####
    out_image = Negative(img)
    .
    #### ورودی ها ####
    img:
    تصویر با درجات خاکستری
    '''

    # مقدار هر پیکسل تصویر نگاتیو برابر است با
    # تفاضل مقدار پیکسل متناظر در تصویر از بیشینه مقادیر پیکسل ها
    return img.max() - img


def Gaussian(img, sigma=1, kernel_size=9):
    '''فیلتر مکانی گوسین
    .
    #### نحوه استفاده ####
    out_image = Gaussian(img)
    .
    #### ورودی ها ####
    img:
    تصویر با درجات خاکستری
    sigma:
    انحراف معیار برای تابع گوسین که به طور پیشفرض مقدار 1 دارد
    kernel_size:
    ابعاد کرنل مورد استفاده که به طور پیشفرض مقدار 9 دارد
    '''

    # ساخت کرنل گوسین با توجه به انحراف معیار و ابعاد کرنل ورودی
    kernel = gaussian_kernel(sigma, size=kernel_size)
    # کانوولو کردن کرنل بر روی تصویر ورودی
    return convolve(img, kernel)


def Laplacian(img, coef=1, mode=0):
    '''فیلتر مکانی لاپلاسین که از یک کرنل 3 در 3 استفاده میکند
    .
    #### نحوه استفاده ####
    out_image = Laplacian(img)
    .
    #### ورودی ها ####
    img:
    تصویر با درجات خاکستری
    coef:
    ضریب که میتواند 1+ یا 1- باشد. به طور پیش فرض مقدار 1 دارد
    mode:
    مقدار 0 به معنی استفاده از کرنل با دوران هایی با پله های افزایشی 90 درجه است و مقدار 1 به معنی استفاده از کرنل با دوران هایی با پله های افزایشی 456 درجه است. به طور پیشفرض مقدار 0 دارد
    '''
    
    # ساخت کرنل لاپلاسین با توجه به مقادیر ورودی
    kernel = laplacian_kernel(coef, mode)
    # کانوولو کردن کرنل بر روی تصویر ورودی
    return convolve(img, kernel)


def DoG(img, sigma1=1, sigma2=1.4, kernel_size=5):
    '''Difference of Gaussians
    .
    #### نحوه استفاده ####
    out_image = DoG(img)
    .
    #### ورودی ها ####
    img:
    تصویر با درجات خاکستری
    sigma1:
    انحراف معیار برای تابع گوسین کرنل اول. به طور پیشفرض مقدار 1 دارد
    sigma2:
    انحراف معیار برای تابع گوسین کرنل دوم. به طور پیشفرض مقدار 1.4 دارد
    kernel_size:
    ابعاد کرنل مورد استفاده که به طور پیشفرض مقدار 5 دارد
    '''
    
    g1 = Gaussian(img, sigma1, kernel_size)
    g2 = Gaussian(img, sigma2, kernel_size)

    return g2 - g1


def LoG(img, sigma=1, kernel_size=3, edge=True):
    '''Laplacian of Gaussian یا Marr
    .
    #### نحوه استفاده ####
    edge = LoG(img)
    or
    out_image = LoG(img, edge=False)
    .
    #### ورودی ها ####
    img:
    تصویر با درجات خاکستری
    sigma:
    انحراف معیار برای تابع گوسین که به طور پیشفرض مقدار 1 دارد
    kernel_size:
    ابعاد کرنل مورد استفاده که به طور پیشفرض مقدار 3 دارد
    edge:
    به طور پیشفرض عملیات لبه یابی انجام میشود. اگر نمیخواهید این اتفاق بیافتد مقدار False را تعیین کنید.
    '''

    # اعمال فیلتر گوسین بر روی تصویر جهت حذف نویز
    g = Gaussian(img, sigma, kernel_size)
    # اعمال فیلتر لاپلاسین بر روی خروجی فیلتر گوسین
    LoG = Laplacian(g)

    # چک میشود که اگر درخواست لبه یابی نشده باشد تصویر حاصل شده از فییلتر لاپلاسین برگردد
    if not edge:
        return LoG
    
    # جهت انجام فرایند لبه یابی، عملیات zero-crossing انجام میشود.
    return zero_crossing(LoG)


def Sobel(img):
    '''فیلتر سوبل با کرنل های 3 در 3
    .
    #### نحوه استفاده ####
    (slope, alpha) = Sobel(img)
    .
    #### ورودی ها ####
    img:
    تصویر با درجات خاکستری
    '''

    # محاسبه کرنل های سوبل در دو جهت ایکس و وای
    Kx, Ky = sobel_kernels()
    
    # کانوولو کردن کرنل ها بر روی تصویر ورودی
    Sx = convolve(img, Kx)
    Sy = convolve(img, Ky)

    # محاسبه شیب
    sobel = np.sqrt(Sx**2 + Sy**2)
    # محاسبه زاویه شیب
    alpha = np.arctan2(Sy, Sx)
 
    return sobel, alpha


def FFT(InputImage, filter_type='LPF', R=0.09, N_Order=1):

    Y = imfft(InputImage)
    if filter_type.lower() == 'lpf':
        kernel = low_pass_kernel(Y.shape, R, N_Order)
    elif filter_type.lower() == 'hpf':
        kernel = high_pass_kernel(Y.shape, R, N_Order)

    Y = kernel * Y
    return imifft(Y)


if __name__ == "__main__":
    from convert import RGB2Gray
    import matplotlib.pyplot as plt
    
    img = RGB2Gray(plt.imread('./data/me.jpg'))
    # r = Laplacian(img)
    # r = DoG(img, kernel_size=10)
    # r = LoG(img, edge=False)
    # r = LoG(img)
    # r, _ = Sobel(img)
    # _, r = Sobel(img)
    
    plt.figure()
    ax = plt.subplot(121)
    plt.imshow(img, 'gray')
    ax.set_title('orginal image')
    ax.axes.get_xaxis().set_ticks([])
    ax.axes.get_yaxis().set_ticks([])
    plt.box(True)
    ax = plt.subplot(122)
    plt.imshow(r, 'gray')
    ax.set_title("Filters")
    ax.axes.get_xaxis().set_ticks([])
    ax.axes.get_yaxis().set_ticks([])
    plt.box(True)

    plt.show()
