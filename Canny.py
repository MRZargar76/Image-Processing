import numpy as np
from Filters import Gaussian, Sobel

class Canny:
    '''لبه یابی با استفاده از فرایند کَنی
    .
    #### نحوه استفاده ####
    edge = Canny().fit(img)
    .
    #### ورودی ها ####
    T_H:
    حد آستانه بالا جهت حل حد آستانه گذاری حاذبه ای. به طور پیشفرض مقدار 0.4 دارد
    T_L:
    حد آستانه پایین جهت حل حد آستانه گذاری حاذبه ای. به طور پیشفرض مقدار 0.1 دارد
    sigma:
    انحراف معیار برای تابع گوسین که به طور پیشفرض مقدار 1.3 دارد
    kernel_size:
    ابعاد کرنل مورد استفاده که به طور پیشفرض مقدار 5 دارد
    '''
    def __init__(self, T_H=0.4, T_L=0.1, sigma=1.3, kernel_size=8):
        self.T_H = T_H
        self.T_L = T_L
        self.sigma = sigma
        self.kernel_size = kernel_size


    def fit(self, img):
        '''لبه یابی
        .
        #### ورودی ها ####
        img:
        تصویر با درجات خاکستری
        '''
        self.img = np.asarray(img)
        self.img_shape = self.img.shape
        
        # اعمال فیلتر گوسین بر روی تصویر ورودی جهت کاهش نویز
        img = Gaussian(self.img, self.sigma, self.kernel_size)
        # محاسبه شیب و زاویه شیب با استفاده از فیلتر سوبل
        img, alpha = Sobel(self.img)
        # حذف غیر بیشینه ها با استفاده از تصاویر شیب و زاویه شیب محاسبه شده
        img = self.__non_maximum_suppression(img, alpha)
        # یافتن لبه های ضعیف و قوی از طریق حدآستانه گذاری جاذبی
        strong, weak = self.__threshold(img)
        # ردیابی لبه از طریق آنالیز blob
        edges = self.__edge_tracking_by_hysteresis(strong, weak)

        return edges


    def __non_maximum_suppression(self, img, alpha):
        '''غیر بیشینه ها با استفاده از تصاویر شیب و زاویه شیب ورودی داده شده، حذف میشود و برگردانده میشود
        .
        ####  ورودی ها  ####
        img:
        شیب تصویر / خروجی فیلتر سوبل
        alpha:
        تصویر زاویه شیب / خروجی فیلتر سوبل
        '''
        
        # محاسبه ابعاد تصویر
        w, h = self.img_shape
        # محاسبه زاویه شیب بر حسب درجه
        alpha *= 180.0 / np.pi
        # انتقال زاویه شیب به 0 تا 180 درجه
        alpha += 180.0
        # نادیده گرفتن پیکسل های کناری جهت جلوگیری از خطا
        alpha = alpha[1:-1,1:-1]

        # محاسبه موقعیت هر پیسکل در راستای ایکس و ایگری بر حسب اندیس
        y, x = np.meshgrid(range(1, h-1), range(1, w-1))
        # یجه مقدار دهی آن ها و تشخیص موقعیت پیسکل همسایه در راستای زاویه شیب
        dx = np.where(np.logical_and(22.5 <= alpha, alpha < 112.5), 1,\
            np.where(np.logical_and(112.5 <= alpha, alpha < 157.5), -1, 0))
        dy = np.where(np.logical_and(22.5 > alpha, alpha >= 157.5), 1,\
            np.where(np.logical_and(67.5 <= alpha, alpha < 112.5), 0, -1))

        # محاسبه بزرگترین مقدار در راستای جهت شیب در هر پکسل (محاسبه بزرگترین مقدار در بین پیکسل مرکزی، پیکسل در جهت شیب و در خلاف جهت شیب)
        m = np.maximum(img[x, y], img[x+dx, y+dy], img[x-dx, y-dy])

        # محاسبه ماتریس بزرگترین مقدار در راستا شیب به ابععاد تصویر ورودی
        dirPixels = np.zeros(self.img_shape)
        dirPixels[1:-1,1:-1] = m

        # حذف غیر بیشینه ها
        return np.where(img >= dirPixels, img, 0)


    def __threshold(self, img):
        '''یافتن لبه های ضعیف و قوی از طریق حدآستانه گذاری جاذبی
        .
        ####  ورودی ها  ####
        img:
        تصویر بیشینه ها با درجات خاکستری
        '''
        
        # محاسبه لبه های قوی بر اساس حدآستانه بالا
        strong = np.where(img >= self.T_H, 1, 0)
        # محاسبه لبه های ضعیف بر اساس حدآستانه پایین
        weak = np.where(img >= self.T_L, 1, 0) - strong

        return strong, weak


    def __edge_tracking_by_hysteresis(self, strong, weak):
        '''ردیابی لبه از طریق آنالیز blob
        .
        ####  ورودی ها  ####
        strong:
        تصویر درجات خاکستری لبه های قوی
        weak:
        تصویر درجات خاکستری لبه های ضعیف
        '''
        
        # محاسبه ابعاد تصویر
        w, h = self.img_shape

        # این عملیات تا شناسایی تمامی لبه های ضعیف متصل به لبه های قوی ادامه پیدا میکند
        repeat = True
        while repeat:
            repeat = False
            for i in range(1, w-1):
                for j in range(1, h-1):
                    # چک میشود که پیکسلی که بررسی میشود جزو لبه های ضعیف باشد
                    if not weak[i,j]:
                        continue
                    # اگر در همسایگی لبه ضعیف وجود نداشته باشد مورد بعدی چک میشود
                    if strong[i-1:i+1,j-1:j+1].any():
                        strong[i, j] = 1
                        weak[i, j] = 0

                        repeat = True

        return strong


if __name__ == "__main__":
    from convert import RGB2Gray
    import matplotlib.pyplot as plt

    img = RGB2Gray(plt.imread('./data/me.jpg'))
    r = Canny(T_H=0.65*255, T_L=0.4*255, sigma=1.5).fit(img)

    plt.figure()
    ax = plt.subplot(121)
    plt.imshow(img, 'gray')
    ax.set_title('orginal image')
    ax.axes.get_xaxis().set_ticks([])
    ax.axes.get_yaxis().set_ticks([])
    plt.box(True)
    ax = plt.subplot(122)
    plt.imshow(r, 'gray')
    ax.set_title("Canny")
    ax.axes.get_xaxis().set_ticks([])
    ax.axes.get_yaxis().set_ticks([])
    plt.box(True)

    plt.show()
