import numpy as np
from common import *
from convert import RGB2Gray


class SIFT:
    '''‫‪Scale-Invariant‬‬ ‫‪Feature‬‬ ‫‪Transform‬‬
    .
    ####  نحوه استفاده  ####
    out = SIFT(img).fit()
    .
    ####  ورودی ها  ####
    img:
    تصویر ورودی
    '''
    def __init__(self, img):
        img = np.asarray(img)
        # اگر تصویر ورودی بیشتر از یک باند داشته باشد به تصویر درجات خاکستری تبدیل میشود
        if img.ndim > 2 and img.shape[2] > 1:
            img = RGB2Gray(img)
        # درجات خاکستری تصویر به بازه ی صفر تا یک آورده میشود
        img = (img - np.min(img)) / (np.max(img) - np.min(img))

        self.img = img
        self.gaussian_kernel_size = 10 # الکی زدم
        self.octave_space_count = 3 # الکی زدم
        self.threshold_for_remove_keypoints = 0.03 
        self.r = 10
        self.threshold_for_eliminate_edge = (self.r + 1)**2 / self.r


    def fit(self, sigma=0.5, ScaleSpace=2):
        '''####  ورودی ها  ####
        sigma:
        انحراف معیار پایه
        ScaleSpace:
        الگوریتم هر اکتاو را به یک عدد اینتیجر تقسیم میکند
        '''
        self.s = int(ScaleSpace)
        self.sigma = sigma

        # محاسبه ظریب انحراف معیار در هر سطح از اکتاو
        self.k = 2**(1/self.s)
        # محاسبه تعداد تصاویر در هر اکتاو
        self.img_count_per_octave = self.s + 3

        img = self.img
        # octave_space = []
        # انجام عملیات بر روی هر یک از اکتاو ها
        for octave_idx in range(self.octave_space_count):
            # محاسبه تصاویر دیفرانسیل گوسین
            DoG_images = self.__generate_diffrance_of_gaussian_images(img, octave_idx)
            # شناسایی اکسترمم های محلی
            keypoints = self.__Detecting_local_exterma(DoG_images, octave_idx)
            # جهت نقاط کلیدی
            # self.__Keypoints_orientation()
            # توصیفگر های نقطه کلیدی
            # self.__()

            # ‫نمودن‬ ‫داونسمپل‬ تصویر
            img = self.__downsapling(img, 2)

        # return octave_space


    def __generate_diffrance_of_gaussian_images(self, img, octave_idx):
        '''محاسبه تصاویر دیفرانسیل گوسین
        .
        ####  ورودی ها  ####
        img:
        تصویر ورودی اکتاو
        octave_idx:
        اندیس اکتاو
        '''
        # محاسبه انحراف معیار پایه ی اکتاو
        sigma = self.sigma * 2**octave_idx

        DoG_images = []
        for DoG_image_idx in range(self.img_count_per_octave - 1):
            # محاسبه دیفرانسیل گوسین
            _DoG = DoG(img, sigma, sigma * self.k, self.gaussian_kernel_size)

            DoG_images.append(_DoG)
            sigma *= self.k
        
        return np.asarray(DoG_images)


    def __Detecting_local_exterma(self, DoG_images, octave_idx):
        '''شناسایی اکسترمم های محلی
        .
        ####  ورودی ها  ####
        DoG_images:
        تصاویر دیفرانسیل گوسین در اکتاو وارده
        octave_idx:
        اندیس اکتاو
        '''
        # یافتن نقاط کلیدی اولیه
        keypoints = self.__Finding_the_initial_keypoints(DoG_images)
        # افزایش دقت محل نقاط کلیدی
        keypoints = self.__Improving_the_Accuracy_of_Keypoints_locations(keypoints, DoG_images, octave_idx)
        # حذف پاسخ های لبه
        keypoints = self.__Eliminating_edge_response(keypoints, DoG_images)

        return keypoints


    def __Finding_the_initial_keypoints(self, DoG_images):
        '''یافتن نقاط کلیدی اولیه
        .
        ####  ورودی ها  ####
        DoG_images:
        تصاویر دیفرانسیل گوسین در اکتاو وارده
        '''
        min_max = []
        for min_max_idx in range(1, self.s + 1):
            # شناسایی مقادیر بیشینه و کمینه پیکسل ها براساس تصاویر دیفرانسیل گوسین اکتاو
            min, max = self.__get_local_minima_and_maxima(DoG_images, min_max_idx)

            min_max.append(min + max)

        return np.asarray(min_max)
    

    def __get_local_minima_and_maxima(self, DoG_images, index):
        '''شناسایی مقادیر بیشینه و کمینه پیکسل ها براساس تصاویر دیفرانسیل گوسین اکتاو
        .
        ####  ورودی ها  ####
        DoG_images:
        تصاویر دیفرانسیل گوسین در اکتاو وارده
        index:
        اندیس تصویر دیفرانسیل گوسین
        '''
        # تصویر مرکزی
        DoG_image = DoG_images[index]
        # ابعاد تصویر
        img_shape = DoG_image.shape

        mins = np.zeros((*img_shape, 3))
        maxs = np.zeros((*img_shape, 3))
        # محاسبه همسایه های هر پیکسل در تصویر مرکزی، بالایی و پایینی
        for i in [-1, 0, 1]:
            # محاسبه همسایگی
            neighbors = get_neighbors_as_shift_images(DoG_images[index +i])
            
            # محاسبه کمینه در همسایگی ها
            mins[:,:, i+1] = np.min(np.min(neighbors, axis=0), axis=0)
            # محاسبه بیشینه در همسایگی ها
            maxs[:,:, i+1] = np.max(np.max(neighbors, axis=0), axis=0)

        # محاسبه مقدار کمینه و بیشینه در همه همسایگی ها
        # تشخیص پیکسل های بیشنیه و کمینه
        min = np.where(DoG_image == np.min(mins, axis=2), 1, 0)
        max = np.where(DoG_image == np.max(maxs, axis=2), 1, 0)

        return min, max


    def __Improving_the_Accuracy_of_Keypoints_locations(self, keypoints_in_octave, DoG_images, octave_idx):
        '''افزایش دقت محل نقاط کلیدی
        .
        ####  ورودی ها  ####
        keypoints_in_octave:
        نقاط کلیدی اولیه در اکتاو
        DoG_images:
        تصاویر دیفرانسیل گوسین در اکتاو وارده
        octave_idx:
        اندیس اکتاو
        '''
        # محاسبه انحراف معیار پایه اکتاو
        sigma = self.sigma * 2**octave_idx

        min_max = []
        for min_max_idx in range(1, self.s + 1):
            keypoints = keypoints_in_octave[min_max_idx-1]

            # اگر نقاط کلیدی وجود نداشته باشد به سراج تصویر نقاط کلیدی بعدی می رویم
            if not keypoints.any():
                continue

            # حذف نقاط کلیدی با کنتراست پایین
            min_max.append(
                self.__Remove_keypoints_with_low_Contrast(
                    keypoints,
                    DoG_images,
                    min_max_idx,
                    sigma
                )
            )

        return np.asarray(min_max)


    def __Remove_keypoints_with_low_Contrast(self, keypoints, DoG_images, index, octave_sigma):
        '''حذف نقاط کلیدی با کنتراست پایین
        .
        ####  ورودی ها  ####
        keypoints:
        تصاویر نقاط کلیدی
        DoG_images:
        تصاویر دیفرانسیل گوسین در اکتاو وارده
        index:
        اندیس تصویر دیفرانسیل گوسین
        octave_sigma:
        انحراف معیار پایه اکتاو
        '''
        # تصویر مرکزی
        DoG_image = DoG_images[index]
        
        # محاسبه همسایه ها
        neighbors = get_neighbors_as_shift_images(DoG_image)

        # محاسبه مشتق مرتبه دوم در راستاهای مختلف
        xDiff = neighbors[1, 2] - neighbors[1, 0]
        yDiff = neighbors[2, 1] - neighbors[2, 0]
        zDiff = DoG_images[index +1] - DoG_images[index -1]

        offset = octave_sigma * (self.k**index - self.k**(index-2))

        # کنتراست
        DxHat = DoG_image + 0.5 * (2 * xDiff + 2 * yDiff + offset * zDiff)
        # DxHat = DoG_image + (xDiff + yDiff + 0.5 * offset * zDiff)

        # حذف نقاط کلیدی با کنتراست پایین
        return np.where(
            np.logical_and(keypoints, np.abs(DxHat) < self.threshold_for_remove_keypoints),
            0,
            keypoints
        )


    def __Eliminating_edge_response(self, keypoints_in_octave, DoG_images):
        '''حذف پاسخ های لبه
        .
        ####  ورودی ها  ####
        keypoints_in_octave:
        نقاط کلیدی اولیه در اکتاو
        DoG_images:
        تصاویر دیفرانسیل گوسین در اکتاو وارده
        '''
        min_max = []
        for min_max_idx in range(1, self.s + 1):
            DoG_image = DoG_images[min_max_idx]
            keypoints = keypoints_in_octave[min_max_idx-1]

            # اگر نقاط کلیدی وجود نداشته باشد به سراج تصویر نقاط کلیدی بعدی می رویم
            if not keypoints.any():
                continue
            
            # حذف نقاط کلیدی بر روی لبه
            min_max.append(
                self.__Remove_keypoints_on_edges(keypoints, DoG_image)
            )
        
        return np.asarray(min_max)

    
    def __Remove_keypoints_on_edges(self, keypoints, DoG_image):
        '''حذف نقاط کلیدی بر روی لبه
        .
        ####  ورودی ها  ####
        keypoints_in_octave:
        نقاط کلیدی
        DoG_image:
        تصویر دیفرانسیل گوسین متناظر
        '''
        # محاسبه همسایه ها
        neighbors = get_neighbors_as_shift_images(DoG_image)

        # محاسبه شیب در راستاهای مختلف
        Dxx = neighbors[1, 2] - 2 * neighbors[1, 1] + neighbors[1, 0]
        Dyy = neighbors[2, 1] - 2 * neighbors[1, 1] + neighbors[2, 0]
        Dxy = neighbors[2, 2] + neighbors[0, 0] - neighbors[0, 2] - neighbors[2, 0]

        # اثر ماتریس
        Tr = Dxx + Dyy
        # دترمینان ماتریس
        Det = Dxx * Dyy - Dxy**2

        # حذف نقاط کلیدی روی لبه
        return np.where(
            np.logical_and(keypoints, Tr**2 / Det >= self.threshold_for_eliminate_edge),
            0,
            keypoints
        )


    def __Keypoints_orientation(self):
        pass


    def __downsapling(self, img, n, m=None):
        '''داونسمپل کردن تصویر
        .
        ####  ورودی ها  ####
        img:
        تصویر ورودی
        n:
        تعداد پیکسل های که ادغام میشوند
        '''
        if m is None:
            m = n

        if img.ndim == 2:
            return img[::n, ::n]
        else:
            return img[::n, ::n, :]
