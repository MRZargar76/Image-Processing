import numpy as np
import matplotlib.pyplot as plt


def fourier_forward_transform_kernel2D(x, y, u, v, N):
    '''محاسبه کرنل هسته تبدیل مستقیم فوریه
    '''
    return np.exp(-2j * np.pi * (u*x + v*y)/N) / N**2


def fourier_inverse_transform_kernel2D(x, y, u, v, N):
    '''محاسبه کرنل هسته تبدیل معکوس فوریه
    '''
    return np.exp(2j * np.pi * (u*x + v*y)/N)


def hartley_transform_kernel2D(x, y, u, v, N):
    '''محاسبه کرنل هسته تبدیل مستقیم/معکوس هارتلی
    '''
    a = 2 * np.pi * u * x / N
    b = 2 * np.pi * v * y / N
    
    return (np.cos(a) + np.sin(a)) * (np.cos(b) + np.sin(b)) / N**2


def cosine_transform_kernel2D(x, y, u, v, N):
    '''محاسبه کرنل هسته مستقیم/معکوس کسینوسی
    '''
    au = np.sqrt(1/N) if u == 0 else np.sqrt(2/N)
    av = np.sqrt(1/N) if v == 0 else np.sqrt(2/N)

    a = (2 * x + 1) * u * np.pi / (2 * N)
    b = (2 * y + 1) * v * np.pi / (2 * N)

    return au * av * np.cos(a) * np.cos(b)


def sine_transform_kernel2D(x, y, u, v, N):
    '''محاسبه کرنل هسته مستقیم/معکوس سینوسی
    '''
    a = (x + 1) * (u + 1) * np.pi / (N + 1)
    b = (y + 1) * (v + 1) * np.pi / (N + 1)

    return np.sin(a) * np.sin(b) * 2 / (N + 1)


def walsh_hadamard_transform_kernel2D(x, y, u, v, N):
    '''محاسبه کرنل هسته مستقیم/معکوس والش هادامارد
    '''
    m = np.log2(N)

    if m % 1 != 0:
        raise 'N must be 2^m'
    m = int(m)

    def b(num):
        return [int(elem) for elem in np.base_repr(num).zfill(m)][::-1]
    def p(num, idx):
        _b = b(num)
        return _b[-1] if idx == 0 else _b[-idx] + _b[-idx-1]

    bx = np.asarray(list(map(b, x.ravel()))).reshape((N, N, m))
    by = np.asarray(list(map(b, y.ravel()))).reshape((N, N, m))
    pu = np.asarray([p(u, i) for i in range(m)])
    pv = np.asarray([p(v, i) for i in range(m)])

    sum = np.sum((bx * pu + by * pv).T, axis=0).T
    
    return (-1)**sum / N


def haar_transform_kernel2D(x, y, u, v, N):
    '''محاسبه کرنل هسته هار
    '''
    def h(X, U, N):
        if U != 0:
            p = np.floor(np.log2(U))
            q = U - 2**p
        else:
            p = q = 0

        X = X / N
        U = U * np.ones((N, N))

        return np.where(
            np.logical_and(U == 0, 0 <= X, X < 1),
            1,
            np.where(
                np.logical_and(q/2**p <= X, X < (q+0.5)/2**p, U > 0),
                2**(p/2),
                np.where(
                    np.logical_and((q+0.5)/2**p <= X, X < (q+1)/2**p, U > 0),
                    -2**(p/2),
                    0
                )
            )
        )

    return h(x,u,N) * h(y,v,N)


def show_base_functions(forward_transform_kernel, N=8):
    '''نمایش توابع پایه برای تبدیل های خطی ورودی
    .
    #### نحوه استفاده ####
    show_base_functions(forward_transform_kernel)
    .
    #### ورودی ها ####
    forward_transform_kernel:
    یک تابع که به یک هسته تبدیل مستقیم اشاره میکند
    N:
    طول اضلاع تصاویر
    '''

    # تشکیل ماتریس ها موقیت
    rng = range(N)
    y, x = np.meshgrid(rng, rng)

    # ایجاد ساختار کلی و جانمایی تصاویر توابع پایه
    fig, ax = plt.subplots(N, N)
    fig.suptitle('Basis Images for ' + ' '.join(forward_transform_kernel.__name__.split('_')[:-1]))
    for u in rng:
        for v in rng:
            # محاسبه تصویر پایه برای مقادیر u و v
            f = forward_transform_kernel(x, y, u, v, N)
            
            # استفاده از مقدار حقیقی تصویر پایه جهت نمایش
            ax[u, v].imshow(f.real, 'gray')
            # شماره گذاری سطر و ستون
            if u == 0: ax[u, v].set_title(str(v))
            if v == 0: ax[u, v].set_ylabel(str(u))
            # حذف لیبل های پلات ها
            ax[u, v].axes.get_xaxis().set_ticks([])
            ax[u, v].axes.get_yaxis().set_ticks([])
            # کشیدن یک خط دور تا دور تصویر
            plt.box(True)
    
    # نمایش تصاویر
    plt.show()


def gram_schmidt(A):
    # grams  Gram-Schmidt orthogonalization of the columns of A.
    # The columns of A are assumed to be linearly independent.
    #
    # Q = grams(A) returns an m by n matrix Q whose columns are 
    # an orthonormal basis for the column space of A.
    #
    # [Q, R] = grams(A) returns a matrix Q with orthonormal columns
    # and an invertible upper triangular matrix R so that A = Q*R.
    # 
    # -------
    # Extracted from `RG Index Algorithm`

    # Published in: Arabian Journal of Geosciences, vol. 9, no. 1, p. 45, Jan. 2016.
    # Title: "Quality assessment of pan-sharpening methods in high-resolution
    # satellite images using radiometric and geometric index"
    # Authors: M. Hasanlou and M. R. Saradjian


    A = A.astype(np.float32)
    _, n = np.shape(A)
    Asave = A.copy()

    for j in range(n):
        for k in range(j):
            mult = (A[:, j].T @ A[:, k]) / (A[:, k].T @ A[:, k])
            A[:, j] = A[:, j] - mult * A[:, k]

    Q = np.empty_like(A)
    for j in range(n):
        if np.linalg.norm(A[:, j]) < np.sqrt(eps):
            raise Exception('Columns of A are linearly dependent. ' + str(j))

        Q[:, j] = A[:, j] / np.linalg.norm(A[:, j])

    R = Q.T @ Asave

    return Q, R


if __name__ == "__main__":
    show_base_functions(fourier_forward_transform_kernel2D)
    # show_base_functions(hartley_transform_kernel2D)
    # show_base_functions(cosine_transform_kernel2D)
    # show_base_functions(sine_transform_kernel2D)
    # show_base_functions(walsh_hadamard_transform_kernel2D)
    # show_base_functions(haar_transform_kernel2D)
