import numpy as np
from convert import RGB2Gray
from convert import hyper_to_2D, twoD_to_hyper, move_between_0_to_1
from scipy.sparse import coo_matrix, linalg
from matplotlib.pyplot import imread


class ThreeD_modeling_based_on_photometric_stereo:
    '''مدل سازی سه بعدی به روش فتومتریک استریو
    .
    #### نحوه استفاده ####
    model = ThreeD_modeling_based_on_photometric_stereo(images, sorce_dimentions)
    model.fit()
    .
    #### ورودی ها ####
    images:
    تصاویر ورودی که می تواند آدرس تصویر یا ماتریس  numpy باشد
    sorce_dimentions:
    جهت منابع نور بایستی به صورت لیست وارد شود.
    '''


    class Source:
        '''منبع نور
        .
        #### ورودی ها ####
        image:
        تصویر ورودی که می تواند آدرس تصویر یا ماتریس  numpy باشد
        direction:
        جهت منبع نور شامل یک بردار در فضای برداری دکارتی.
        '''


        def __init__(self, image, direction):
            self.__set_image(image)
            self.__set_direction(direction)


        def __set_image(self, image):
            '''دریافت تصویر
            '''

            # اگر ورودی با فرمت استرینگ باشد، انتظار میرود مسیر ذخیره سازی تصویر وارد شده باشد
            if isinstance(image, str):
                image = imread(image)

            # ثبت اطلاعات کلی تصویر
            self.nRow, self.nCol = image.shape[:2]
            self.nPixel = self.nRow * self.nCol

            # تبدیل تصویر از حالت سه بعدی و معمول به دیتاست دو بعدی (هر داده در یک ردیف و فیچرها در ستون)
            self.image = hyper_to_2D(image)
            self.gray_img = hyper_to_2D(RGB2Gray(image, False) / 255)


        def __set_direction(self, direction):
            '''ثبت جهت منبع نور نسبت به شی
            '''

            direction = np.asarray(direction)
            self.direction = direction.ravel()


    def __init__(self, images, sorce_dimentions):
        # چک میشود که تعداد تصاویر با تعداد منابع نور برابر باشد
        assert len(images) == len(sorce_dimentions),\
            'images count not equal with sorce_dimentions count'

        # ایجاد منابع
        self.sources = [self.Source(img, dir) for img, dir in zip(images, sorce_dimentions)]

        # تبت اطلاعات کلی تصاویر
        self.source_count = len(self.sources)
        self.nPixel = self.sources[0].nPixel
        self.nRow = self.sources[0].nRow
        self.nCol = self.sources[0].nCol

        self.__is_fit__ = False


    def fit(self, all_output=False):
        '''محاسبه نقاط سه بعدی
        .
        #### ورودی ها ####
        all_output:
        مشاهده تمام خروجی ها
        '''

        # محاسبه بردار نرمال ها
        self._N, self._rmse_N = self.__get_normal_vectors()
        # محاسبه ارتفاع نقاط
        self._DEM, self.norm_rmse_DEM = self.__get_DEM()

        self.__is_fit__ = True

        return self.DEM


    def __get_normal_vectors(self):
        '''محاسبه بردار نرمال ها
        '''
        # G = S * N

        normals = []
        rmse = []

        # آماده سازی ماتریس منابع نور(S)
        # توجه شود که ماتریس S برای همه ی نقاط یکسان هستند.
        S = [source.direction for source in self.sources]
        S = np.asarray(S)

        # محاسبه بردار نرمال برای هر پیکسل(نقطه)
        for pixel_idx in range(self.nPixel):
            # تشکیل ماتریس G بر اساس درجات خاکستری تصاویر متناظر هر منبع نوری
            G = [source.gray_img[pixel_idx] for source in self.sources]
            G = np.asarray(G)

            # محاسبه بردار نرمال به روش کمترین مربعات
            N = np.linalg.inv(S.T @ S) @ S.T @ G

            # محاسبه وزن مناسب برای کمترین مربعات بر اساس خطای اولیه محاسبه شده
            e = G - S @ N
            w = np.diag((1 /  e**2).ravel())

            # محاسبه بردار نرمال به روش کمترین مربعات و وزن محاسبه شده
            N = np.linalg.inv(S.T @ w @ S) @ S.T @ w @ G
            # یکه کردن بردار نرمال
            N /= np.linalg.norm(N)

            normals.append(N)
            # محاسبه remse بردار نرمال محاسبه شده
            e = G - S @ N
            rmse.append(np.sqrt(np.mean(e**2)))

        return normals, rmse


    def __get_DEM(self):
        '''محاسبه ارتفاع نقاط
        '''
        # B = A * Z

        # شماره دهی هر یکی از پیکسل ها
        index = twoD_to_hyper(np.arange(self.nPixel), self.nRow, self.nCol)
        # انتخاب پیکسل های مرکزی
        center = hyper_to_2D(index[1:, :-1]).ravel()
        # انتخاب پیکسل سمت راست
        right = hyper_to_2D(index[1:, 1:]).ravel()
        # انتخاب پیکسل بالایی
        top = hyper_to_2D(index[:-1, :-1]).ravel()

        B = []
        A_pos = []
        # محاسبه ضرایب معادلات و تعیین موقعیت مقادیر در ماتریس
        for i, pixel_idx in enumerate(center):
            # دریافت بردار نرمال متناظر پیکسل
            n = self._N[pixel_idx].ravel()

            B.append( -n[0]/n[2] )
            B.append( -n[1]/n[2] )

            A_pos.append([2*i, pixel_idx, -1])
            A_pos.append([2*i, right[i], 1])
            A_pos.append([2*i+1, pixel_idx, -1])
            A_pos.append([2*i+1, top[i], 1])

        # Z0 = 0
        # مقدار دهی ارتفاع اولین پیکسل(بالا سمت چپ) برابر صفر
        B.append(0)
        A_pos.append([2*i+2, 0, 1])

        # Z_top_right_corner = 0
        # مقدار دهی ارتفاع پیکسل گوشه بالا سمت راست برابر صفر(چون در هیچ یک از معادلات قرار نگرفته است)
        B.append(0)
        A_pos.append([2*i+3, index[0,-1], 1])

        A_pos = np.asarray(A_pos)

        # تشکیل ماتریس های تنک
        A = coo_matrix((A_pos[:,2], (A_pos[:,0], A_pos[:,1])))
        B = np.asarray(B)

        # محاسبه ارتفاع نقاط با کمترین مربعات
        result = linalg.lsqr(A, B)
        Z, r1norm = result[0], result[3]

        return Z, r1norm


    @property
    def N(self):
        '''بردار نرمال ها'''
        return twoD_to_hyper(self._N, self.nRow, self.nCol)


    @property
    def DEM(self):
        '''مدل ارتفاعی نقاط'''
        return twoD_to_hyper(self._DEM, self.nRow, self.nCol)


    @property
    def rmse_N(self):
        '''مدل باقی مانده بردار نرمال ها'''
        return twoD_to_hyper(self._rmse_N, self.nRow, self.nCol)


    def show_normals(self, fromRow=None, toRow=None, fromColumn=None, toColumn=None):
        if not self.__is_fit__:
            raise 'object don\'t fit...'
        
        if fromRow is None:
            fromRow = 0
        if fromColumn is None:
            fromColumn = 0
        if toRow is None:
            toRow = self.nRow
        if toColumn is None:
            toColumn = self.nCol
        if fromRow >= toRow or fromColumn >= toColumn:
            raise 'from > to !!'

        import matplotlib.pyplot as plt

        Xrng = range(self.N.shape[0])
        Yrng = range(self.N.shape[1])
        y, x = np.meshgrid(Yrng, Xrng)

        u, v = self.N[:,:, 0], self.N[:,:, 1]

        # fig, ax = plt.subplots()
        # ax.quiver(
        #     x[fromRow:toRow, fromColumn:toColumn],
        #     y[fromRow:toRow, fromColumn:toColumn],
        #     u[fromRow:toRow, fromColumn:toColumn],
        #     v[fromRow:toRow, fromColumn:toColumn]
        # )
        # ax.set_title("Normal vectors")

        plt.figure()
        plt.subplot(121)
        plt.imshow(self.N)
        plt.title("Normals")
        plt.subplot(122)
        plt.imshow(self.rmse_N)
        plt.title("RMSE")
        plt.colorbar()

        plt.show()


    def show_dem(self):
        if not self.__is_fit__:
            raise 'object don\'t fit...'

        import matplotlib.pyplot as plt

        plt.figure()
        plt.imshow(self.DEM, 'gray')
        plt.title("DEM")
        plt.show()


    def show_3d_model(self):
        if not self.__is_fit__:
            raise 'object don\'t fit...'

        import matplotlib.pyplot as plt
        
        Xrng = range(self.N.shape[0])
        Yrng = range(self.N.shape[1])
        y, x = np.meshgrid(Yrng, Xrng)
        
        plt.figure()
        ax = plt.axes(projection='3d')
        ax.scatter(x, y, self.DEM, c=self.__get_mean_of_source_images(), cmap='viridis', linewidth=0.5)

        plt.show()


    def __get_mean_of_source_images(self):
        images = np.asarray([source.image for source in self.sources])

        return np.mean(images, axis=0) / 255


if __name__ == "__main__":
    folder_path = './data/photometric_stereo/'

    image_paths = [
        folder_path + 'img1.jpg',
        folder_path + 'img2.jpg',
        folder_path + 'img3.jpg',
        folder_path + 'img4.jpg',
    ]

    image_directions = np.loadtxt(
        fname=folder_path + 'light_directions.txt',
        delimiter=','
    )

    a = ThreeD_modeling_based_on_photometric_stereo(image_paths, image_directions)
    a.fit()
    a.show_dem()
    a.show_normals()
    a.show_3d_model()
    print()
