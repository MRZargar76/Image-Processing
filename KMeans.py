import random
import numpy as np
from convert import hyper_to_2D
from convert import twoD_to_hyper


class KMeans:
    '''خوشه بندی تصویر با استفاده از روش k-means
    .
    #### نحوه استفاده ####
    out_image = KMeans(img).fit()
    .
    #### ورودی ها ####
    img:
    تصویر ورودی
    '''

    class Center:
        def __init__(self, values, neighbors=None):
            self.values = values
            self.group = []
            self.neighbors = neighbors

        def add_members(self, members):
            self.group.extend(members)
        
        def add_member(self, member):
            self.group.append(member)

        def update(self, values, neighbors=None):
            self.values = values
            self.group = []
            if neighbors is not None:
                self.neighbors = neighbors

        @property
        def len_group(self):
            return len(self.group)

        @property
        def is_empty_group(self):
            return self.len_group == 0
        
        def __add__(self, other):
            return np.array(self.values) + np.array(other.values)

        def __sub__(self, other):
            return np.array(self.values) - np.array(other.values)


    def __init__(self, img):
        img = np.asarray(img)
        
        # تصویر ورودی بایستی دو بعدی یا سه بعدی باشد
        if img.ndim not in [2, 3]:
            raise 'image shape must be similar to (m, n[, k])'
        
        self.shape = img.shape
        # به خاطر نحوه پردازش تصویر دو بعدی به سه بعدی مجازی تبدیل میشود 
        if img.ndim == 2:
            self.shape += (1,)
        # تصویر به بر اساس تعداد باند به یک مستطیل داده تبدیل میشود
        # به طوری که سطر ها تعیین کنند پیکسل و ستون ها باند های تصویر هستند
        self.img = hyper_to_2D(img)
        self.pixel_count = self.img.shape[0]
        self.pixels_idx = np.asarray(range(self.pixel_count))
        
        if self.img.ndim == 1:
            temp = np.empty((self.pixel_count, 1))
            temp[:, 0] = self.img
            self.img = temp


    def fit(self, k=5, repeat=None, threshold_for_change=5):
        '''خوشه بندی تصویر
        .
        #### ورودی ها ####
        k:
        تعداد دسته
        repeat:
        تعداد تکرار
        threshold_for_change:
        حدآستانه دقت عملیات
        '''
        self.k = k

        # تعیین مقادیر اولیه مراکز دسته
        self.init_centers()
        
        # اگر تعداد تکرار عملیات تعیین نشده باشد فرایند تا زمانی که دقت از حدآستانه تعیین شده کمتر نشود ادامه می یابد
        if repeat is None:
            repeat = np.inf

        ago_centers = None
        counter = 0
        while True:
            counter += 1

            # بروزرسانی مراکز دسته
            self.update_centers()
            # شناسایی پیکسل های متعلق به هر دسته
            self.assign_pixels()
            
            print(self.calculate_error(ago_centers))
            # اگر به تعداد کافی تکرار شده باشد عملیات متوقف میشود
            if counter >= repeat:
                break
            # اگر دقت عملیات به اندازه کافی کاهش پیدا کرده باشد، عملیات متوقف میشود
            if self.calculate_error(ago_centers) < threshold_for_change:
                break

            # مراکز دسته جهت محاسبه دقت عملیات کپی میشود
            ago_centers = self.copy_from_centers
        
        # مقادیر پیکسل های تصویر بر اساس مقادیر مراکز دسته آنها رنگ بندی میشوند
        self.update_image()

        # تصویر به شکل اولیه از نظر ابعاد بر میگردد
        return np.asarray(twoD_to_hyper(self.img, *self.shape[:2]), dtype=np.uint8)


    @property
    def copy_from_centers(self):
        '''کپی از مراکز دسته
        '''
        return [self.Center(center.values) for center in self.centers]


    def update_image(self):
        '''مقدار دهی تصویر اولیه بر اساس مقادیر مراکز دسته نظیر آنها
        '''
        for center in self.centers:
            # با توجه به این که در مرحله نهایی بعد از شناسایی پیکسل های نظیر هر دسته مراکز دسته اگر بروزرسانی نمیشوند، مقادیر مراکز دسته محاسبه میشود
            values = self.calculate_values_of_center(center)

            # مقدار دهی پیکسل های دسته
            self.img[center.group] = np.round(values)


    def calculate_error(self, ago_centers):
        '''محاسبه خطای عملیات بر اساس مراکز دسته قبلی
        .
        #### ورودی ها ####
        ago_centers:
        مراکز دسته قبلی
        '''

        # اگر مراکز دسته قبلی موجود نباشد خطا بینهایت تلقی میشود
        if ago_centers is None: 
            return np.inf
        
        # خطای هر دسته = جذر مجموع مربعات تفاضلات مقادیر دسته قبل و حال حاضر
        # خطای کلی = جذر مجموع مربعات خطای هر دسته
        sum = 0
        for idx in range(self.k):
            ago = ago_centers[idx]
            new = self.centers[idx]

            sum += np.sum(np.array(new - ago)**2)

        return np.sqrt(sum)


    def init_centers(self):
        '''مقدار دهی اولیه مراکز دسته
        '''
        # تعداد ویژگی/باند ها
        feature_count = self.shape[2]
        # کمینه مقدار در هر ویژگی/باند
        min = np.min(self.img, axis=0)
        # بیشینه مقدار در هر ویژگی/باند
        max = np.max(self.img, axis=0)
            
        def random_pixel_value():
            '''محاسبه مقادیر تصادفی بر اساس دامنه تغییرات هر ویژگی/باند
            '''
            if feature_count == 1:
                return random.randint(min, max)
            
            return [random.randint(min[i], max[i]) for i in range(feature_count)]
        
        # مقدار دهی مراکز دسته
        # توجه شود که عدم تعیین همسایگی های هر دسته به منرله انتخاب کل تصویر به عنوان همسایگی های هر دسته هستند
        self.centers = [self.Center(random_pixel_value()) for _ in range(self.k)]


    def get_update_neighbors(self, center):
        '''بروزرسانی همسایگی های مرکز دسته
        '''
        # در روش k-means همسایگی های هر دسته ثابت اند
        return center.neighbors


    def calculate_values_of_center(self, center):
        '''محاسبه مقادیر دسته بر اساس پیکسل های دسته
        .
        ####  ورودی ها  ####
        center:
        مرکز دسته
        '''
        # مقادیر هر دسته بر اساس میانگین مقادیر پیکسل های وابسته به دسته هستند
        return np.mean(self.img[center.group], axis=0)


    def update_centers(self):
        '''بروزرسانی مراکز دسته
        '''

        for center in self.centers:
            # اگر هیچ پیکسلی وابسته به مرکز دسته نباشد به سراغ دسته بعدی میرویم
            if center.is_empty_group:
                continue
            
            # محاسبه مقادیر دسته
            values = self.calculate_values_of_center(center)
            # تعیین همسایگی های هر دسته
            neighbors = self.get_update_neighbors(center)
            # اعمال تغییرات
            center.update(values, neighbors)


    def dist(self, center, neighbors):
        '''محاسبه فاصله پیکسل های همسایه از مرکز دسته
        .
        ####  ورودی ها  ####
        center:
        مقادیر مرکز دسته
        neighbors:
        پیکسل های همسایه
        '''
        # فاصله = فاصله اقلیدسی
        return np.sqrt(np.sum((center - neighbors)**2, axis=1))


    def minimum_distance(self):
        '''تعیین نزدیکترین مرکز دسته به هر یک از پیکسل ها
        '''
        argmin = np.zeros(self.pixel_count, dtype=int) -1
        empty_distance_list = lambda: np.ones(self.pixel_count) * np.inf

        # محاسبه فاصله همسایه ها از تک تک مراکز دسته و انتخاب کمینه آنها
        min_distance = empty_distance_list()
        for i in range(self.k):
            center = self.centers[i]

            distance = empty_distance_list()

            # اگر همسایه ها تعیین نشده باشند کل تصویر به عنوان همسایه دسته در نظر گرفته میشود
            if center.neighbors is None:
                neighbors_idx = self.pixels_idx
                neighbors = self.img
            else:
                neighbors_idx = center.neighbors
                neighbors = self.img[neighbors_idx]

            # محاسبه فاصله پیکسل/همسایه ها از مرکز دسته
            distance[neighbors_idx] = self.dist(center.values, neighbors)
            # انتخاب کمینه نسبت به فواصل کمینه قبلی
            argmin = np.where(distance < min_distance, i, argmin)

            # محاسبه فواصل کمینه تابه این مرحله
            min_distance = np.where(distance < min_distance, distance, min_distance)

        return argmin


    def assign_pixels(self):
        '''شناسایی پیکسل های متعلق به هر دسته
        '''
        # تعیین نزدیکترین مرکز دسته به هر یک از پیکسل ها
        center_idx_with_min_distance = self.minimum_distance()
        
        # اضافه کردن پیکسل های وابسته به تک تک دسته ها
        for center_idx in range(self.k):
            pixels = self.pixels_idx[center_idx_with_min_distance == center_idx]
            
            self.centers[center_idx].add_members(pixels)


if __name__ == "__main__":
    import matplotlib.pyplot as plt
    
    img = plt.imread('./data/me.jpg')
    r = KMeans(img).fit(k=5, threshold_for_change=0.5)
    
    plt.figure()
    ax = plt.subplot(121)
    plt.imshow(img)
    ax.set_title('orginal image')
    ax.axes.get_xaxis().set_ticks([])
    ax.axes.get_yaxis().set_ticks([])
    plt.box(True)
    ax = plt.subplot(122)
    plt.imshow(r)
    ax.set_title("KMeans")
    ax.axes.get_xaxis().set_ticks([])
    ax.axes.get_yaxis().set_ticks([])
    plt.box(True)

    plt.show()
