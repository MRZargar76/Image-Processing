import numpy as np


def padding_image(img, right=0, left=0, top=0, bottom=0):
    '''گسترش تصویر در راستاهای مختلف
    در واقع تصویر آینه وار تکرار میشود
    .
    ####  نحوه استفاده  ####
    out_image = padding_image(img, right=right, left=left, top=top, bottom=bottom)
    .
    ####  ورودی ها  ####
    img:
    تصویر ورودی
    right:
    تعداد پیکسل در سمت راست تصویر
    left:
    تعداد پیکسل در سمت چپ تصویر
    top:
    تعداد پیکسل در سمت بالا تصویر
    bottom:
    تعداد پیکسل در سمت پایین تصویر
    .
    '''
    # محاسبه ابعاد تصویر
    w, h = np.shape(img)
    
    # اگر تعداد پیکسل هایی که از هر سمت از ابعاد تصویر بزرگتر باشد به صورت بازگشیتی از تصویر گسترش یافته استفاده میشود
    if right > w:
        img = padding_image(img, right=w)
        right -= w
    if left > w:
        img = padding_image(img, left=w)
        left -= w
    if top > h:
        img = padding_image(img, top=h)
        top -= h
    if bottom > h:
        img = padding_image(img, bottom=h)
        bottom -= h

    # محاسبه ابعاد تصویر جدید
    w_new, h_new = w + top + bottom, h + right + left

    # ساخت تصویر خالی به ابعاد تصویر خروجی
    result = np.zeros((w_new, h_new))
    # مقدار دهی تصویر خروجی با تصویر اولیه در جایگاه خود
    result[top:-bottom, left:-right] = img

    # تصویر آینه ای در راستای ایکس
    # x axis
    mirror = img[::-1, :]

    # مقدار دهی تصویر خروجی با تصویر آینه ای در سمت های بالا و پایین تصویر ورودی
    result[:top, left:-right] = mirror[-top:, :]
    result[-bottom:, left:-right] = mirror[:bottom, :]

    # تصویر آینه ای در راستای ایگری
    #  y axis
    mirror = result[:, left:-right]
    mirror = mirror[:, ::-1]

    # مقدار دهی تصویر خروجی با تصویر آینه ای در سمت های چپ و راست تصویر وردی
    result[:, :left] = mirror[:, -left:]
    result[:, -right:] = mirror[:, :right]

    return result


def get_neighbors_as_shift_images(img, window_size=(3,3)):
    '''با استفاده از شیفت تصویر به سمت های مختلف با توجه به پنجره همسایگی ورودی، همسایه های هر پیکسل را بر میگرداند
    .
    ####  نحوه استفاده  ####
    neighbors = get_neighbors_as_shift_images(img)
    .
    ####  ورودی ها  ####
    img:
    تصویر ورودی
    window_size:
    ابعاد پنجره همسایگی
    '''
    # محاسبه نصف ابعاد پنجره همسایگی
    wk, hk = window_size
    hwk, hhk = (wk // 2, hk // 2)

    # گسترش تصویر به اندازه نصف پنجره همسایگی در هر چهار سمت
    img = padding_image(img, right=hhk, left=hhk, top=hwk, bottom=hwk)

    neighbors = []
    # محاسبه همسایه ها به ازای هر پیکسل پنجره همسایگی
    for i in range(wk):
        row = []
        for j in range(hk):
            # محاسبه اندیس انتهایی
            w_end = -2*hwk+i
            h_end = -2*hhk+j
            # با توجه به شرایط مقختلف مقدار دهی تفاوت دارد
            if w_end == 0 and h_end != 0:
                neighbor = img[i:, j:h_end]
            elif w_end != 0 and h_end == 0:
                neighbor = img[i:w_end, j:]
            elif w_end == 0 and h_end == 0:
                neighbor = img[i:, j:]
            else:
                neighbor = img[i:w_end, j:h_end]
            
            row.append(neighbor)
        neighbors.append(row)

    return np.asarray(neighbors)


def convolve(img, kernel):
    '''کانوولو کردن کرنل بر روی تصویر
    .
    ####  نحوه استقاده  ####
    out_image = convolve(img, kernel)
    .
    ####  نحوه استقاده  ####
    img:
    تصویر ورودی
    kernel:
    کرنل
    '''
    kernel = np.asarray(kernel)
    
    # محاسبه همسایگی های هر پیکسل به ابعاد کرنل ورودی
    neighbors = get_neighbors_as_shift_images(img, kernel.shape)

    # اعمال کرنل بر روی تصویر
    # اعمال کرنل شامل مجموع حاصل ضرب های مقادیر هر کرنل در همسایگی های کرنل است
    sum = np.zeros(img.shape)
    for i in range(kernel.shape[0]):
        for j in range(kernel.shape[1]):
            sum += neighbors[i, j] * kernel[i, j]
    
    return sum
