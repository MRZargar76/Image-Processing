import numpy as np


def hyper_to_2D(img):
    """
    Converts an HSI cube to a 2D matrix
    Converts a 3D HSI cube (m x n x p) to a 2D matrix of points (N X p)
    where N = m*n, p := number of bands
    
    Usage
      [dataset] = hyperConvert2d(img)
    Inputs
      img - 3D HSI cube (m x n x p)
    Outputs
      img - 2D data matrix (N x p)
    """
    img = np.array(img)
    ndim = img.ndim

    assert ndim in [2, 3],\
      "dimension of image is invalid. ndim = 2 or 3"

    if ndim == 2:
      m, n = img.shape
      
      hyper_img = np.empty((m,n,1), dtype=img.dtype)
      hyper_img[:,:,0] = img

      return hyper_to_2D(hyper_img)

    m, n, p = img.shape
    N = n * m
    
    dataset = img.T.reshape((p, N)).T

    return dataset


def twoD_to_hyper(dataset, m, n):
  """
  Converts a 2D matrix to an HSI cube
  Converts a 2D matrix of points (N X p) to a 3D HSI cube (m x n x p)
  where N = m*n, p := number of bands
  
  Usage
    [img] = twoD_to_hyper(dataset, m, n)
  Inputs
    dataset - 2D data matrix (N x p)
    m - number of rows
    n - number of columns
  Outputs
    img - 3D HSI cube (m x n x p)
  """
  dataset = np.array(dataset)
  ndim = dataset.ndim

  if ndim not in [1, 2]:
    if ndim == 3 and dataset.shape[2] == 1:
      dataset = dataset[:,:,0]
    else:
      raise "dimension of image is invalid. ndim = 1 or 2"

  shape = dataset.shape

  if shape[0] != m * n:
    raise "dimension of image is invalid. please check 'm' and 'n' parameters"

  if ndim == 1:
    dataset_2d = np.empty((m*n, 1), dtype=dataset.dtype)
    dataset_2d[:, 0] = dataset

    return twoD_to_hyper(dataset_2d ,m,n)[:,:,0]
  
  p = shape[1]
  return dataset.T.reshape(p, n, m).T


def to_gray(img, is_unit8=True):
    # لازم است که چک شود ابعاد تصویر ورودی درست باشد.
    if img.ndim not in [2,3]:
        raise 'shape of your image do not support..., img.shape is (m, n) or (m, n, k)'
    if img.ndim == 2:
        return img

    # پیکسل های معادل در باند های مختلف با یک دیگر جمع بسته می شوند
    # مجموع محاسبه شده بر تعداد باند ها تقسیم می شود
    # عدد صحیح مقدار بدست آمده در هر پیکسل محاسبه می شود
    img = np.sum(img, axis=2) / img.shape[2]

    if is_unit8:
      return np.floor(img)
    
    return img


def RGB2Gray(img, is_unit8=True):
    '''از تصویر با سه باند رنگی، تصویر درجات خاکستری آن را بر میگرداند.
    .
    #### نحوه استفاده ####
    out_image = RGB2Gray(img)
    .
    #### ورودی ها ####
    img:
    تصویر با سه باند رنگی قرمز، سبز و آبی
    '''

    # لازم است که چک شود ابعاد تصویر ورودی درست باشد.
    if img.ndim != 3 and img.shape[2] != 3:
        raise 'shape of your image do not support..., img.shape must be (m, n, 3)'

    return to_gray(img, is_unit8)


def move_between_0_to_1(dataset):
	min = np.min(dataset)
	max = np.max(dataset)

	return (dataset - min) / (max - min)


def threshold(img, t):
    '''از تصوير درجه خاکستري ورودي، با توجه به حد ستانه ورودي يک تصوير بولين خروجي داده ميشود.
    .
    ####  نحوه استفاده  ####
    out_image = threshold(img, t)
    .
    ####  ورودي ها  ####
    img:
    تصوير با درجات خاکستري
    t:
    حد آستانه درجات خاکستري
    '''
    return np.where(img > t, 1, 0)
