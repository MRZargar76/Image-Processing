import numpy as np
from convert import threshold
from Filters import Negative
from common import get_neighbors_as_shift_images


class Morphology:
    '''شمامیتوانید با استفاده از مچموعه از اپراتور ها و عملگر های مورفولوری بر روی تصویر خود پردازش انجام دهید
    توجه داشته باشید که استفاده مجدد از شیء ساخته شده به منزله پردازش بر روی تصویر خروجی در پردازش قبلی است.
    .
    #### نحوه استفاده ####
    operator = Morphology(img)
    img = operator.Dilation()
    img = operator.Erosion()
    img = operator.Opening()
    img = operator.Closing()
    img = operator.Hit_or_Miss(kernel)
    .
    #### ورودی ها ####
    img:
    تصویر ورودی
    cmap:
    تنها میتواند به صورت باینری یا درجات خاکستری باشد. به طور پیش فرض تصویر باینری تصور می شود. 'binary' یا 'gray'
    '''
    def __init__(self, img, cmap='binary'):
        self.img = np.asarray(img)
        self.img_shape = self.img.shape
        self.cmap = cmap


    def __based_morphology(self, Dilation_or_Erosion : str, kernel):
        '''فرایند کلی برای اپراتور های پایه مورفولوژی را انجام میدهد
        .
        #### نحوه استفاده ####
        img = __based_morphology('dilation', kernel)
        img = __based_morphology('erosion', kernel)
        .
        #### ورودی ها ####
        Dilation_or_Erosion:
        تعیین نوع اپراتور
        kernel:
        کرنل مورد نظر جهت اعمال اپراتور بر روی تصویر. به طور پیش فرض یک کرنل 3 در 3 با مقادیر یک در نظر گرفته شده است
        توجه داشته باشید که کرنل ورودی تنها میتواند شامل 1 یا None باشد
        '''

        # بررسی میشود که تصویر ورودی باینری یا از توع درجات خاکستری باشد
        if self.cmap not in ['binary', 'gray']: raise 'cmap not define'
        # اگر نوع تصویر ورودی باینری تلقی شود تصویر بر مبنای میانگین درجات خاکستری باینتری میشود
        if self.cmap == 'binary':
            m = np.average(self.img)
            self.img = threshold(self.img, m)
        
        # بررسی میشود که مقادیر کرنل ورودی از مقادیر مناسب تشکیل شده باشند
        if np.any([elem != 1 and elem is not None for elem in kernel.ravel()]):
            raise 'kernel must be from "1" and "None" values'
        
        # نوع اپراتور شناسایی شده و بر اساس آن عملگر های مورد نیاز تعیین میشوند
        if Dilation_or_Erosion.lower() in ['d', 'dilation']:
            # اگر عملگر از نوع dilation باشد از عملگر های «بیشینه» یا «یا» استفاده میشود
            # بنابراین تصویر خروجی اولی مقدار «صفر» خواهند داشت
            result = np.zeros(self.img_shape)
            if self.cmap == 'gray':
                # اگر تصویر ورودی از نوع درجات خاکستری باشد عملگر «بیشینه» استفاده میشود
                decision_function = np.maximum
            else:
                # اگر تصویر ورودی از نوع درجات خاکستری باشد عملگر «یا» استفاده میشود
                decision_function = np.logical_or
        elif Dilation_or_Erosion.lower() in ['e', 'erosion']:
            # اگر عملگر از نوع erosion باشد از عملگر های «کمینه» یا «و» استفاده میشود
            # بنابراین تصویر خروجی اولی مقدار «یک» خواهند داشت
            result = np.ones(self.img_shape) * np.inf
            if self.cmap == 'gray':
                # اگر تصویر ورودی از نوع درجات خاکستری باشد عملگر «کمینه» استفاده میشود
                decision_function = np.minimum
            else:
                # اگر تصویر ورودی از نوع درجات خاکستری باشد عملگر «و» استفاده میشود
                decision_function = np.logical_and
        else:
            raise 'type not define'
        
        # دریافت همسایگی های هر پیکسل بر اساس اندازه کرنل ورودی
        neighbors = get_neighbors_as_shift_images(self.img, kernel.shape)

        # اعمال کرنل بر روی تصویر
        for i in range(kernel.shape[0]):
            for j in range(kernel.shape[1]):
                kernel_elem = kernel[i, j]
                neighbor = neighbors[i, j]

                # اگر مقدار کرنل از نوع None باشد به معنای نادیده گرفتن است
                if kernel_elem is None:
                    continue
                
                # اعمال عملگر در نظر گرفته شده بر روی همسایگی های نظیر مقدار کرنل
                applied_kernel = neighbor * kernel_elem
                result = decision_function(result, applied_kernel)

        self.img = result
        return self.img


    def __morphology(self, kernel):
        '''شناسایی پیکسل هایی از تصویر که منطبق بر کرنل ورودی است
        این عملیات تنها برای تصاویر باینری استفاده میشود.
        kernel:
        کرنل
        توجه داشته باشید که مقادیر کرنل تنها میتواند شامل 1، 0 و یا None باشد
        '''
        # تبدیل تصویر ورودی به تصویر باینری بر اساس میانگین درجات خاکستری
        m = np.average(self.img)
        self.img = threshold(self.img, m)
        
        # بررسی میشود که مقادیر کرنل ورودی از مقادیر مناسب تشکیل شده باشند
        if np.any([elem not in [0, 1] and elem is not None for elem in kernel.ravel()]):
            raise 'kernel must be from "0", "1" and "None" values'

        # دریافت همسایگی های هر پیکسل بر اساس اندازه کرنل ورودی
        neighbors = get_neighbors_as_shift_images(self.img, kernel.shape)

        # با توجه به استفاده از عملگر «و« در این عملیات تصویر خروجی اولیه شامل مقادیر «یک» خواهند بود
        result = np.ones(self.img.shape)
        # اعمال کرنل بر روی تصویر
        for i in range(kernel.shape[0]):
            for j in range(kernel.shape[1]):
                kernel_elem = kernel[i, j]
                neighbor = neighbors[i, j]

                # اگر مقدار کرنل از نوع None باشد به معنای نادیده گرفتن است
                if kernel_elem is None:
                    continue
                
                # شناسایی همسایگی منطبق با کرنل
                applied_kernel = neighbor == kernel_elem
                # اعمال عملگر
                result = np.logical_and(result, applied_kernel)

        self.img = result
        return self.img


    def Dilation(self, kernel=None, repeat=1):
        '''یکی از دو اپراتورهای پایه در مورفولوژی ریاضی
        .
        #### نحوه استفاده ####
        out_image = Dilation(img)
        .
        #### ورودی ها ####
        kernel:
        کرنل مورد نظر جهت اعمال اپراتور بر روی تصویر. به طور پیش فرض یک کرنل 3 در 3 با مقادیر یک در نظر گرفته شده است
        توجه داشته باشید که کرنل ورودی تنها میتواند شامل 1 یا None باشد
        repeat:
        تعداد تکرار اپراتور بر روی تصویر ورودی
        '''

        # اگر کرنل ورودی تعیین نشده باشد، کرنل پیش فرض تعیین میشود
        if kernel is None:
            kernel = np.ones((3,3))

        kernel = np.array(kernel)

        # اعتبار سنجی تعداد تکرار اپراتور بر روی تصویر
        if repeat < 0:
            raise 'repeat count error'
        
        # کنترل تکرار عملیات
        for i in range(repeat):
            # اعمال اپراتور بر روی تصویر با توجه به کرنل موجود
            self.img = self.__based_morphology('d', kernel)
        
        return self.img


    def Erosion(self, kernel=None, repeat=1):
        '''یکی از دو اپراتورهای پایه در مورفولوژی ریاضی
        .
        #### نحوه استفاده ####
        out_image = Erosion(img)
        .
        #### ورودی ها ####
        kernel:
        کرنل مورد نظر جهت اعمال اپراتور بر روی تصویر. به طور پیش فرض یک کرنل 3 در 3 با مقادیر یک در نظر گرفته شده است
        توجه داشته باشید که کرنل ورودی تنها میتواند شامل 1 یا None باشد
        repeat:
        تعداد تکرار اپراتور بر روی تصویر ورودی
        '''

        # اگر کرنل ورودی تعیین نشده باشد، کرنل پیش فرض تعیین میشود
        if kernel is None:
            kernel = np.ones((3,3))
        
        # اعتبار سنجی تعداد تکرار اپراتور بر روی تصویر
        if repeat < 0:
            raise 'repeat count error'

        # کنترل تکرار عملیات
        for i in range(repeat):
            # اعمال اپراتور بر روی تصویر با توجه به کرنل موجود
            self.__based_morphology('e', kernel)
        
        return self.img


    def Opening(self, kernel=None, repeat=1):
        '''Openning
        .
        #### نحوه استفاده ####
        out_image = Opening(img)
        .
        #### ورودی ها ####
        kernel:
        کرنل مورد نظر جهت اعمال اپراتور بر روی تصویر. به طور پیش فرض یک کرنل 3 در 3 با مقادیر یک در نظر گرفته شده است
        توجه داشته باشید که کرنل ورودی تنها میتواند شامل 1 یا None باشد
        repeat:
        تعداد تکرار اپراتور بر روی تصویر ورودی
        '''

        # اعمال اپراتور Erosion بر روی تصویر
        self.Erosion(kernel, repeat)
        # اعمال اپراتور Dilation بر روی خروجی عملیات قبلی
        self.Dilation(kernel, repeat)

        return self.img


    def Closing(self, kernel=None, repeat=1):
        '''Closing
        .
        #### نحوه استفاده ####
        out_image = Closing(img)
        .
        #### ورودی ها ####
        kernel:
        کرنل مورد نظر جهت اعمال اپراتور بر روی تصویر. به طور پیش فرض یک کرنل 3 در 3 با مقادیر یک در نظر گرفته شده است
        توجه داشته باشید که کرنل ورودی تنها میتواند شامل 1 یا None باشد
        repeat:
        تعداد تکرار اپراتور بر روی تصویر ورودی
        '''

        # اعمال اپراتور Dilation بر روی تصویر
        self.Dilation(kernel, repeat)
        # اعمال اپراتور Erosion بر روی خروجی عملیات قبلی
        self.Erosion(kernel, repeat)

        return self.img


    def Hit_or_Miss(self, kernel1, kernel2=None, repeat=1):
        '''ابزاری پایه برای شناسایی شکل است
        .
        #### نحوه استفاده ####
        out_image = Hit_or_Miss(img, kernel1)
        .
        #### ورودی ها ####
        kernel1:
        کرنل اول. توجه داشته باشید که مقادیر کرنل تنها میتواند شامل 1، 0 و یا None باشد
        kernel2:
        کرنل اول. توجه داشته باشید که مقادیر کرنل تنها میتواند شامل 1، 0 و یا None باشد
        توجه داشته باشید که اگر کرنل دوم تعیین نشود تصویر ورودی تنها میتواند به صورت باینری باشد
        repeat:
        تعداد تکرار اپراتور بر روی تصویر ورودی
        '''
        
        # اگر کرنل دوم تعیین نشده باشد عملیات به گونه ای دیگر انجام میشود
        if kernel2 is None:
            # بررسی نوع تصویر ورودی که درصورتی که تصویر باینری نباشد خطا برگردانده میشود
            if self.cmap != 'binary':
                raise 'if you have just one kernel, must use from binary image'

            # اعمال کرنل اول به تعداد خواسته شده
            for i in range(repeat):
                self.__morphology(kernel1)

            return self.img

        # در صورتی که دو کرنل تعریف شده باشد، خروجی حاصل از اشتراک خروجی های دو اپراتور Erosion هست
        copy_img = self.img.copy()
        e1 = self.Erosion(kernel1, repeat)
        self.img = Negative(copy_img)
        e2 = self.Erosion(kernel2, repeat)

        # بررسی نوع تصویر ورودی جهت انتخاب تابع مناسب جهت اشتراک گیری 
        if self.cmap == 'binary':
            # اگر تصویر باینری باشد اشتراک گیری با استفاده از عملگر منطقی AND انجام میشود
            f = np.logical_and
        elif self.cmap == 'gray':
            # اگر تصویر درجه خاکستری باشد اشتراک گیری با استفاده از مینیمم گیری انجام میشود
            f = np.minimum
        
        # اشتراک گیری
        self.img = f(e1, e2)
        return self.img


if __name__ == "__main__":
    from convert import RGB2Gray
    import matplotlib.pyplot as plt
    
    img = RGB2Gray(plt.imread('./data/me.jpg'))
    r = Morphology(img).Dilation(repeat=3)
    # r = Morphology(img, 'gray').Dilation(repeat=3)
    # r = Morphology(img).Erosion(repeat=3)
    # r = Morphology(img, 'gray').Erosion(repeat=3)
    # r = Morphology(img).Opening(repeat=3)
    # r = Morphology(img, 'gray').Opening(repeat=3)
    # r = Morphology(img).Closing(repeat=3)
    # r = Morphology(img, 'gray').Closing(repeat=3)
    # k = np.array([[0, 0, None],[0,1, 1],[None,1, None]])
    # r = Morphology(img).Hit_or_Miss(k)
    
    plt.figure()
    ax = plt.subplot(121)
    plt.imshow(img, 'gray')
    ax.set_title('orginal image')
    ax.axes.get_xaxis().set_ticks([])
    ax.axes.get_yaxis().set_ticks([])
    plt.box(True)
    ax = plt.subplot(122)
    plt.imshow(r, 'gray')
    ax.set_title("Morphology")
    ax.axes.get_xaxis().set_ticks([])
    ax.axes.get_yaxis().set_ticks([])
    plt.box(True)

    plt.show()
